package com.example.joni.kulujenseuranta;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class ExpenseList extends Fragment {
    private ListView lv;
    private AppDatabase db;

    public ExpenseList() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_expense_list, container, false);

        db = ((MainActivity)getActivity()).getDBConnection();
        lv = (ListView) view.findViewById(R.id.expenseList);
        List<Expense> expenses = db.expenseDao().getAll();
        List<String> expenseStrings = new ArrayList<String>();

        for(Expense kulu : expenses){
            expenseStrings.add(kulu.getDescription().toString()+" - "+Double.toString(kulu.getAmount()));
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),  android.R.layout.simple_list_item_1, expenseStrings);
        lv.setAdapter(adapter);

        // Inflate the layout for this fragment
        return view;
    }

}

package com.example.joni.kulujenseuranta;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ExpenseDao {

    @Query("SELECT * FROM expense")
    List<Expense> getAll();

    @Insert
    void insertAll(Expense... expense);

    @Delete
    void delete(Expense expense);
}

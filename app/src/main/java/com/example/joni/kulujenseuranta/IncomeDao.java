package com.example.joni.kulujenseuranta;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface IncomeDao {

    @Query("SELECT * FROM income")
    List<Income> getAll();

    @Insert
    void insertAll(Income income);

    @Delete
    void delete(Income income);
}

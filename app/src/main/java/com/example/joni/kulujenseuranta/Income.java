package com.example.joni.kulujenseuranta;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Income {
    @PrimaryKey(autoGenerate = true)
    private int incomeID;
    @ColumnInfo(name = "amount")
    private double amount;
    @ColumnInfo(name = "description")
    private String description;

    public Income(){
    }

    public Income(double amount, String description){
        this.amount = amount;
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIncomeID() {
        return incomeID;
    }

    public void setIncomeID(int incomeID) {
        this.incomeID = incomeID;
    }
}

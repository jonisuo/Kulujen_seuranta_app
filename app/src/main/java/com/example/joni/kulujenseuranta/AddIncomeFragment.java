package com.example.joni.kulujenseuranta;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class AddIncomeFragment extends Fragment {
    private AppDatabase db;
    private EditText incomeDescInput;
    private EditText incomeAmountInput;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_incomeform, container, false);

        db = ((MainActivity)getActivity()).getDBConnection();

        incomeDescInput = view.findViewById(R.id.incomeDescInput);
        incomeAmountInput = view.findViewById(R.id.incomeAmountInput);

        Button incomeSaveBtn = view.findViewById(R.id.incomeSaveBtn);
        incomeSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newDesc;
                double newAmount;

                newDesc = incomeDescInput.getText().toString();
                newAmount = Double.parseDouble(incomeAmountInput.getText().toString());

                Income newIncome = new Income(newAmount, newDesc);
                db.incomeDao().insertAll(newIncome);

                Toast.makeText(getActivity(), "Income added",
                        Toast.LENGTH_LONG).show();
            }
        });

        return view;
    }
}

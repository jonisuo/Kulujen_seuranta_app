package com.example.joni.kulujenseuranta;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends AppCompatActivity {
    private AppDatabase db;
    private static MainActivity appContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appContext = this;
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "KuluseurantaDB").allowMainThreadQueries().build();  //allowMainThreadQueries tulisi muuttaa toisenlaiseksi

        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }

            FrontpageFragment newFragment = new FrontpageFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.fragment_container, newFragment);
            transaction.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.expensesListMenu) {
            changeFragment(3);
        }
        else if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void changeFragment(int fragmentid){

        if (fragmentid == 1){
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            AddExpenseFragment expenseFragment = new AddExpenseFragment();
            transaction.addToBackStack(null);
            transaction.replace(R.id.fragment_container, expenseFragment)
                    .commit();
        }

        else if (fragmentid == 2){
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            AddIncomeFragment incomeFragment = new AddIncomeFragment();
            transaction.addToBackStack(null);
            transaction.replace(R.id.fragment_container, incomeFragment)
                    .commit();
        }
        else if (fragmentid == 3){
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            ExpenseList expenseListFragment = new ExpenseList();
            transaction.addToBackStack(null);
            transaction.replace(R.id.fragment_container, expenseListFragment)
                    .commit();
        }
        else if (fragmentid == 4){
            DialogFragment newFragment = new DatepickerFragment();
            newFragment.show(getSupportFragmentManager(), "datePicker");
        }
    }

    public AppDatabase getDBConnection(){
        return db;
    }

    public static MainActivity getAppContext(){
        return appContext;
    }
}

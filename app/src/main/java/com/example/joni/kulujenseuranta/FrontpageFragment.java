package com.example.joni.kulujenseuranta;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class FrontpageFragment extends Fragment {
    private AppDatabase db;

    public FrontpageFragment(){
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_frontpage, container, false);

        FloatingActionButton expenseBtn = view.findViewById(R.id.addExpenseBtn);
        expenseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).changeFragment(1);
            }
        });

        FloatingActionButton incomeBtn = view.findViewById(R.id.addIncomeBtn);
        incomeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).changeFragment(2);
            }
        });

        db = ((MainActivity)getActivity()).getDBConnection();

        return view;
        }
}

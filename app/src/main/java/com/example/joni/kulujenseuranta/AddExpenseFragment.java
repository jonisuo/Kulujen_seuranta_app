package com.example.joni.kulujenseuranta;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;


public class AddExpenseFragment extends Fragment {
    private AppDatabase db;
    private EditText expenseDescInput;
    private EditText expenseAmountInput;
    private RadioButton expenseRadEntertainment;
    private RadioButton expenseRadFood;
    private RadioButton expenseRadBill;
    private RadioButton expenseRadTransportation;
    private RadioButton expenseRadOther;

    public AddExpenseFragment(){
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_expenseform, container, false);

        db = ((MainActivity)getActivity()).getDBConnection();

        expenseDescInput = view.findViewById(R.id.expenseDescInput);
        expenseAmountInput = view.findViewById(R.id.expenseAmountInput);
        expenseRadEntertainment = view.findViewById(R.id.expenseRadEntertainment);
        expenseRadFood = view.findViewById(R.id.expenseRadFood);
        expenseRadBill = view.findViewById(R.id.expenseRadBill);
        expenseRadTransportation = view.findViewById(R.id.expenseRadTransportation);
        expenseRadOther = view.findViewById(R.id.expenseRadOther);

        Button expenseDateBtn = view.findViewById(R.id.expenseDateBtn);
        expenseDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).changeFragment(4);
            }
        });

        Button expenseSaveBtn = view.findViewById(R.id.expenseSaveBtn);
        expenseSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newDesc;
                double newAmount;
                int newType = 0;

                newDesc = expenseDescInput.getText().toString();
                newAmount = Double.parseDouble(expenseAmountInput.getText().toString());
                if(expenseRadEntertainment.isChecked()) newType = 1;
                if(expenseRadFood.isChecked()) newType = 2;
                if(expenseRadBill.isChecked()) newType = 3;
                if(expenseRadTransportation.isChecked()) newType = 4;
                if(expenseRadOther.isChecked()) newType = 5;

                if(newType == 0){
                    Toast.makeText(getActivity(), "You must choose expense type",
                            Toast.LENGTH_LONG).show();
                }
                else{
                    Expense newExpense = new Expense(newAmount, newDesc, newType);
                    db.expenseDao().insertAll(newExpense);
                    Toast.makeText(getActivity(), "Expense added",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        return view;
    }
}

Projektin tavoitteena oli luoda mobiilisovellus, jolla voi pitää kirjaa päivittäisistä henkilökohtaisista tuloista ja menoista.

Sovelluksella on yksi aktiviteetti ja näkymät ovat koko näytön täyttäviä fragmentteja.
Tietokannan käytössä hyödynnetään Room Persistence kirjastoa.